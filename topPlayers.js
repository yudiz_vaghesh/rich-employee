// define array of some names which we will used to generate random name for user from this
var uName = [
    'Vaghesh',
    'Vrund',
    'Brijesh',
    'Bhargav',
    'Tejas',
    'Virat',
    'Sachin',
    'Dhoni'
]

// Math.random() returns a floating point between 0 to 1
// The use pf Math.floor() always gives us integer number
// After Math.random() we specify that the generated number is not less than min and greater than max
var randomGenerator = (min,max) =>  Math.floor(Math.random() * (max - min) + min)

// create function for creating random users based on the count
var createUsers = (count)=>{
    for(var i=0;i<count;i++)
    {
        var myName = uName[randomGenerator(1,uName.length)]
        var myLevel = randomGenerator(1,9);
        var myXp = randomGenerator(0,800);
        var myAge = randomGenerator(20,50);
        var user = {
           NAME : myName,
           AGE  : myAge,
           XP   : myXp,
           LEVEL: myLevel  
        }

        // insert each user in randomusers collection of db test, because we run our script on mongoShell
        db.randomusers.insert(user);
        // console.log(myName,myLevel,myXp,myAge);
    }
}

// call the function
createUsers(1000);

// NOTE: In mongoShell Go at Project folder and run below command to create random profiles of user
// ' mongo <Script_File> '


// Command for finding Top 9 Players along with random User himself

db.randomusers.aggregate([
    {
        $facet:
           {
               "Top9":[{$addFields:{"Score":{$divide:["$XP","$LEVEL"]}}},{$sort:{"Score":-1}},{$limit:9}],
               "SelfUser":[{$sample:{size:1}},{$addFields:{"selfUserScore":{$divide:["$XP","$LEVEL"]}}}]
           }
   },
   {$project:{"TopPlayers":{$setUnion:["$Top9","$SelfUser"]}}},
   {$unwind:"$TopPlayers"},
   {$sort:{"Score":-1}},
   {$replaceWith:"$TopPlayers"},
   {$sort:{Score:-1}}
   ])
   