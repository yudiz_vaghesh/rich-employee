const express = require('express');
const Employee = require('../models/employeeDetails');
var router = express.Router();

// Testing GET route
router.get('/',(req,res)=>{
    res.send('Hello in new tasks');
});

// POST method to add new employees
router.post('/add', async(req,res)=>{
    let { name,salary,managerID } = req.body;

    let employee = new Employee({
        // _id: id,
        sName: name,
        nSalary: salary,
        iManagerId: managerID
    });

    employee = await employee.save();
    res.send(employee);
});

// GET method for getting employee who has more earning than his manager
router.get('/richemployee',async(req,res)=>{
    let employee = await Employee.aggregate([
        {
            $lookup:{
                from:"employees",
                as:"richName",
                let:{"manager":"$iManagerId","salary":"$nSalary"},
                pipeline:[
                    // {$match:{$expr:{$eq:["$_id","$$manager"]}}}
                    // {$match:{$expr:{$gt:["$$manager.nSalary",80000]}}}
                    {$match:{$expr:{$and:[{$eq:["$_id","$$manager"]},{$gt:["$$salary","$nSalary"]}]}}}
                    // {$group:{"_id":"$$ROOT.sName"}},
                    // {$project:{sName:1}}
                    
                ]
            }
        }
        ,{$unwind:"$richName"}
        ,{$group:{"_id":"$sName"}}
        // },
        // {$group:{"_id":"$_id"}}

        // {$match:{"_id":null}}
    ])

    res.send(employee);
});
// Testing for getting manager details from emplyee's data
// router.get('/:id', async (req,res)=>{
    // { $where: "this. a.Salary  > this. b.Salary "}
//     const emp = await Employee.findOne({iManagerId:req.params.id});
//     const man = await Employee.findOne({_id:emp.iManagerId})
//     res.send(man);
// });
module.exports = router;