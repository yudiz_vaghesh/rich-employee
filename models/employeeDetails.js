const mongoose = require('mongoose');


const employees = new mongoose.Schema({
    // _id: {
    //     type: mongoose.Schema.Types.ObjectId
    // },
    sName:{
        type: String,
        required: true
    },
    nSalary:{
        type: Number,
        required: true
    },
    iManagerId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    }
});

const Employee = mongoose.model('employee',employees);

module.exports = Employee;