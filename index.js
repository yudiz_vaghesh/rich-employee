const express = require('express');
const bodyParser = require('body-parser');
const employeeRoutes = require('./routes/employeeRoute');

// intialization of application
var app = express();


// use the bodyParser for form or post data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extendedL:false}));


// import middlewares
app.use('/employee',employeeRoutes);
require('./middlewares/dbConnection');

// run localhost server
app.listen(8080,()=>{
    console.log('Localhost started on 8080.');
})


