// for better performamce we have to create TEXT index on country field
// Steps to getting city name of specific country with limits 5
// 1. Create TEXT index on country field
// 2. whatever data we matched push it into array of one field
// 3. output only 5 documents using limit 


// Below code is work only for specific country
        db.region.aggregate([
            {$match:{country:"China"}}, 
            {$limit:5},
            {$group:
                {
                    _id:"$country", 
                    cities: {$push:{name:"$name"}}
                }
            }
            
        ])

// {$match:{country:"India"}},

//  Below Code is works for each company
    db.cities.aggregate([
        
        {$group:{_id:"$country"}},
        {$lookup:
            {
                from:"cities",
                let:{ "cname":"$_id"},
                pipeline:[
                    {$match:{$expr:{$eq:["$country","$$cname"]}}},
                    {$limit: 5},
                    {$project:{name:1,_id:0}}
                ],
                as:"cities"
            }
        }
    ])

// Below code also gives same output but it is the optimise way of above queries

db.cities.aggregate([
    {
        $group:
        {
            _id:"$country", 
            cities:{$push:{"name":"$name"}}
        }
    },
    {
        $project:{cities:{$slice:["$cities",0,5]}}
    }

])
